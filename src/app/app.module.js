"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var app_routing_1 = require("./app.routing");
var tablepager_component_1 = require("./00_tablepager/tablepager.component");
var login_component_1 = require("./01_login/login.component");
var forgotten_pw_component_1 = require("./01_forgottenpassword/forgotten-pw.component");
var registration_component_1 = require("./01_registration/registration.component");
var registration2_component_1 = require("./01_registration/registration2.component");
var success_reg_component_1 = require("./01_registration/success-reg.component");
var training_new_component_1 = require("./02_training_new/training-new.component");
var users_component_1 = require("./02_users/users.component");
var clientDetails_component_1 = require("./02_clientDetails/clientDetails.component");
var clients_component_1 = require("./02_clients/clients.component");
var setup_component_1 = require("./02_setup/setup.component");
var dashboard_component_1 = require("./02_dashboard/dashboard.component");
var crm_footer_component_1 = require("./02_crm-footer/crm-footer.component");
var crm_header_component_1 = require("./02_crm-header/crm-header.component");
var app_component_1 = require("./app.component");
var profile_component_1 = require("./03_profile/profile.component");
var welcome_component_1 = require("./02_welcome/welcome.component");
var map_component_1 = require("./03_map/map.component");
var trainings_component_1 = require("./02_trainings/trainings.component");
var user_service_1 = require("./services/user.service");
var message_service_1 = require("./services/message.service");
var getlist_training_new_service_1 = require("./services/getlist.training-new.service");
var userDetails_component_1 = require("./03_UserDetails/userDetails.component");
var trainingDetails_component_1 = require("./02_trainingDetails/trainingDetails.component");
var location_pipe_1 = require("./pipes/location.pipe");
var address_pipe_1 = require("./pipes/address.pipe");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_1.routes, http_1.HttpModule],
        declarations: [app_component_1.AppComponent,
            tablepager_component_1.TablePagerComponent,
            forgotten_pw_component_1.ForgottenPwComponent,
            login_component_1.LoginComponent,
            registration_component_1.RegistrationComponent,
            registration2_component_1.RegistrationComponent2,
            success_reg_component_1.SuccessReg,
            training_new_component_1.TrainingNewComponent,
            dashboard_component_1.DashboardComponent,
            crm_footer_component_1.FooterComponent,
            crm_header_component_1.HeaderComponent,
            profile_component_1.ProfileComponent,
            users_component_1.UsersComponent,
            clients_component_1.ClientsComponent,
            setup_component_1.SetupComponent,
            welcome_component_1.WelcomeComponent,
            map_component_1.MapComponent,
            trainings_component_1.TrainingsComponent,
            address_pipe_1.AddressPipe,
            location_pipe_1.LocationPipe,
            clientDetails_component_1.ClientDetailsComponent,
            userDetails_component_1.UserDetailsComponent,
            trainingDetails_component_1.TrainingDetails],
        bootstrap: [app_component_1.AppComponent],
        providers: [user_service_1.UserService, getlist_training_new_service_1.NewTrainingService, message_service_1.MessageService]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map