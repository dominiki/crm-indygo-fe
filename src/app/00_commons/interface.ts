export interface KeyValueObj {
	key : string,
	value : any
}

export const genders : KeyValueObj[] = [
	{ key : 'MALE', value : 'Férfi' },
	{ key : 'FEMALE', value : 'Nő' }
];

export const roles : KeyValueObj[] = [
	{ key : 'ROLE_ADMIN', value : 'Adminisztrátor' },
	{ key : 'ROLE_ASSISTANT', value : 'Ügyintéző' },
	{ key : 'ROLE_TRAINER', value : 'Tanár' }
//	{ key : 'STUDENT', value : 'Hallgató' }
];

export interface Address {
	zip?: number;
	city?: string;
	id?: string;
	street?: string;
	number?: string; //mert lehet például: 154b
	other?: string;	
};

export interface RegistrationStep2Response {
	user: User;
	locations?: Location[];
};

export interface Location {
	address?: Address;
	room?: string;
	selected?: boolean;
};

export interface CourseSearch { 
	courses: any;	
	pageCount: number;
}

export interface CourseEvent {	//ezek a kurzuson belüli tréningek
	name: string;	
	eventId?: string;
	description: string;
	teachers?: User [];
	location?: Location;
	dateStart?: string;
	dateFinish?: string;
};

export interface Course {	//Ha csak 1 CourseEvent van, akkor is egy kurzus része lesz
  	courseName: string;
 	courseDescr: string;
  	courseId?: string;
  	applicationDeadline?: string;
  	maxStudents?: number;
  	minStudents?: number;
  	courseDateStart?: string;	//csak lekérésként fogjuk látni
	courseDateFinish?: string;	//csak lekérésként fogjuk látni
  	discount?: string;
	price?: number;
	priceDeadline?: string;
	events?: CourseEvent[];
 	types?: string[];	//ezek lesznek a tagek (setupban lehet kezelni), amiket előtte le kell kérdezni->BE
 	selected?: boolean;	//ez csak a selectes html részeknél kell az nGmodelhez
};

export interface UserFee {
	id?: string;		//BE Lekéréssel kapjuk meg
	paidFee?: number;
	feeExpiry?: string;
}

export interface UserSearch { 
	users: any;	
	pageCount: number;
}

export interface User {
	name: string;
	email: string;
	id?: string;
	gender?: string;
	birthPlace?: string;
	birthDate?: string;
	phone?: string;
	address?: Address;
	role?: string[];
	username?: string;
//	subjects?: string [];	// (setupban lehet kezelni)
	locations?: Address [];
	actCourses?: Course [];	
	appliedCourses?: Course [];	
	previousCourses?: Course [];
	otherInformation?: string;	//uj erdeklodoknel lehtosegek feltuntetese
	status?: string;
	fee?: UserFee[];
};
export interface SearchFilter {
}
export interface ClientSimpleSearchFilter extends SearchFilter {
	s : string;
}
export interface ClientCombinedSearchFilter extends SearchFilter {
	name : string;
	email : string;
	status : string;
}
export interface TableHeader {
	key : string;
	text : string;
	align?: string;
}
export interface SearchResult {
	data : any[];
	pageCount : number;
}