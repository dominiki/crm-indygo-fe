"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var setup_constans_1 = require("../00_commons/setup-constans");
var user_service_1 = require("../services/user.service");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
var WelcomeComponent = (function () {
    function WelcomeComponent(userService, router, trainingService) {
        this.userService = userService;
        this.router = router;
        this.trainingService = trainingService;
        this.guestList = [];
        this.courseList = [];
        this.currentUser = {
            name: "",
            email: "",
            id: "",
            role: []
        };
        this.actClients = 0;
        this.inquirer = 0;
        this.clientPageCount = 0;
        this.coursePageCount = 0;
        this.currentClientPage = 1;
        this.currentCoursePage = 1;
        this.clientPages = [];
        this.coursePages = [];
        this.itemsPerPage = setup_constans_1.VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerClientPage = this.itemsPerPage[setup_constans_1.VariableSetup.defaultItemsPerPageNumbersIndex].key;
        this.currentItemsPerCoursePage = this.itemsPerPage[setup_constans_1.VariableSetup.defaultItemsPerPageNumbersIndex].key;
    }
    WelcomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        document.body.className = "";
        this.userService.getEveryUserList().then(function (users) { return _this.guestList = users; });
        this.userService.getCurrentUser().then(function (user) { return _this.currentUser = user; });
    };
    WelcomeComponent.prototype.pagesList = function (pagesCount) {
        var pages = [];
        for (var i = 1; i <= pagesCount; i++) {
            pages.push(i);
        }
        return pages;
    };
    WelcomeComponent.prototype.getNeededClientPage = function (pageNo) {
        var _this = this;
        if (pageNo >= 1 && pageNo <= this.clientPageCount) {
            this.userService.getClientsToDashboard(this.currentClientPage, this.currentItemsPerClientPage).then(function (result) {
                _this.guestList = result.users;
                _this.clientPageCount = result.pageCount;
            });
            this.currentClientPage = pageNo;
        }
    };
    WelcomeComponent.prototype.getNeededCoursePage = function (pageNo) {
        var _this = this;
        if (pageNo >= 1 && pageNo <= this.coursePageCount) {
            this.userService.getCoursesToDashboard(this.currentCoursePage, this.currentItemsPerCoursePage).then(function (result) {
                _this.courseList = result.courses;
                _this.coursePageCount = result.pageCount;
            });
            this.currentCoursePage = pageNo;
        }
    };
    WelcomeComponent.prototype.navigateToClient = function (userId) {
        this.router.navigate(['/profile', userId]); //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    };
    WelcomeComponent.prototype.navigateToCourse = function (courseId) {
        this.router.navigate(['/course', courseId]); //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    };
    WelcomeComponent.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.currentUser.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    return WelcomeComponent;
}());
WelcomeComponent = __decorate([
    core_1.Component({
        selector: 'welcome',
        templateUrl: './welcome.component.html',
        styleUrls: ['../../../assets/css/welcome.component.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router, getlist_training_new_service_1.NewTrainingService])
], WelcomeComponent);
exports.WelcomeComponent = WelcomeComponent;
//# sourceMappingURL=welcome.component.js.map