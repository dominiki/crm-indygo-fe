"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var user_service_1 = require("../services/user.service");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var ClientsComponent = (function () {
    function ClientsComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.filterDataCombined = {
            name: '',
            email: '',
            status: '',
        };
        this.filterDataSimple = {
            s: '',
        };
        this.simplySearch = true;
        this.lastFilterData = this.filterDataSimple;
        this.currentUser = {
            name: "",
            email: "",
            id: "",
            status: ""
        };
        this.status = [];
    }
    ClientsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getStatus().then(function (status) { _this.status = status; });
        this.userService.getCurrentUser().then(function (user) { return _this.currentUser = user; });
    };
    ClientsComponent.prototype.saveSimple = function () {
        this.lastFilterData = this.filterDataSimple;
    };
    ClientsComponent.prototype.saveCombined = function () {
        this.lastFilterData = this.filterDataCombined;
    };
    ClientsComponent.prototype.doClientSearch = function (searchFilter, page, itemsPerPage) {
        return (this.lastFilterData['s'] !== undefined) ? this.simpleSearchUser(searchFilter, page, itemsPerPage) : this.combinedSearchUser(searchFilter, page, itemsPerPage);
    };
    ClientsComponent.prototype.simpleSearchUser = function (searchFilter, page, itemsPerPage) {
        return this.userService.getUsersSimpleSearch(searchFilter, page, itemsPerPage);
    };
    ClientsComponent.prototype.combinedSearchUser = function (searchFilter, page, itemsPerPage) {
        return this.userService.getUsersCombinedSearch(searchFilter, page, itemsPerPage);
    };
    ClientsComponent.prototype.navigateToUser = function (data) {
        var userId = data.id;
        this.router.navigate(['/user', userId]); //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    };
    ClientsComponent.prototype.changeSearch = function () {
        this.simplySearch = !this.simplySearch;
    };
    ClientsComponent.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.currentUser.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    return ClientsComponent;
}());
ClientsComponent = __decorate([
    core_1.Component({
        selector: 'clients',
        templateUrl: './clients.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router])
], ClientsComponent);
exports.ClientsComponent = ClientsComponent;
//# sourceMappingURL=clients.component.js.map