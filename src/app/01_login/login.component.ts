import { Component, OnInit, Injectable } from '@angular/core';
import { Http,  HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../constant';
import { User } from '../00_commons/interface';


@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
 	
	username: string;
	password: string;
	errorText: string;

	
	constructor (private http: Http, private router: Router ) {
		this.username = "user";
		this.password = "userPass";
		this.errorText = "";
	}

	ngOnInit () {
		document.body.className = "login"; 
	}

	doLogin(): void {
		this.errorText = '';
		if (this.username == '') {
			this.errorText = 'Adja meg a felhasználónevét!';
			return;
		} else if (this.password == '') {
			this.errorText = 'Adja meg a jelszavát!';
			return;
		}else{
			// http://10.10.1.15:8080/crm_indygo-1.0-SNAPSHOT/crm/
			let data = new URLSearchParams();
			data.append('username', this.username);
			data.append('password', this.password);

			this.http.post(
				Constants.BASEURL + 'login',
				data,
				{ withCredentials : true } )
			.toPromise()
			.then( ()=>{
			this.router.navigate(['dashboard']);
			})
			.catch( ( response )=>{
				console.log(response);
				if(response.status == 0) {
					this.errorText = "Hálozati hiba!";
				}
				else if(response.status >=400 && response.status < 500) {
					this.errorText = "Érvénytelen felhasználónév vagy jelszó!"
					this.router.navigate(['login']);
				}else{
					this.errorText = "Szerverhiba!";
					this.router.navigate(['login']);
				}
				  
			});
		}
		

	}
}