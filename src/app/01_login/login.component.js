"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var constant_1 = require("../constant");
var LoginComponent = (function () {
    function LoginComponent(http, router) {
        this.http = http;
        this.router = router;
        this.username = "user";
        this.password = "userPass";
        this.errorText = "";
    }
    LoginComponent.prototype.ngOnInit = function () {
        document.body.className = "login";
    };
    LoginComponent.prototype.doLogin = function () {
        var _this = this;
        this.errorText = '';
        if (this.username == '') {
            this.errorText = 'Adja meg a felhasználónevét!';
            return;
        }
        else if (this.password == '') {
            this.errorText = 'Adja meg a jelszavát!';
            return;
        }
        else {
            // http://10.10.1.15:8080/crm_indygo-1.0-SNAPSHOT/crm/
            var data = new http_1.URLSearchParams();
            data.append('username', this.username);
            data.append('password', this.password);
            this.http.post(constant_1.Constants.BASEURL + 'login', data, { withCredentials: true })
                .toPromise()
                .then(function () {
                _this.router.navigate(['dashboard']);
            })
                .catch(function (response) {
                console.log(response);
                if (response.status == 0) {
                    _this.errorText = "Hálozati hiba!";
                }
                else if (response.status >= 400 && response.status < 500) {
                    _this.errorText = "Érvénytelen felhasználónév vagy jelszó!";
                    _this.router.navigate(['login']);
                }
                else {
                    _this.errorText = "Szerverhiba!";
                    _this.router.navigate(['login']);
                }
            });
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'login',
        templateUrl: './login.component.html'
    }),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map