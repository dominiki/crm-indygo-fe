import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { SearchFilter, TableHeader, SearchResult } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';


@Component({
	selector: 'table-pager',
	templateUrl: './tablepager.component.html',
})
export class TablePagerComponent implements OnInit {


	@Input()
	searchFilter : SearchFilter;
	@Input()
    searchFunction: (searchFilter : SearchFilter, page: number, itemsPerPage : number) => Promise<SearchResult>;
    @Input()
    emptyResultText : string;
    @Input()
    tableHeaders : TableHeader[];
    @Input()
    owner : Component;

    @Output()
    rowClick = new EventEmitter<any>();

	pageCount: number;    //BE-től kapott érték, hogy a lapozóban hány oldal lesz
    currentPage: number;    //a fenti oldalak közül épp melyik oldalon vagyok
    pages: number[];        //ngFor-hoz kell, a pageCount értékéig töltöm fel számokkal 1-től
    itemsPerPage: any[];    //BE-lekérés, ami mutatja, hogy a selectorban milyen értékek legyenek
    currentItemsPerPage: number;    //az előző közül kiválasztott érték
    tableData : any[];

    constructor() {
    	this.pageCount = 0;
        this.currentPage = 1;
        this.pages = []; 
        this.itemsPerPage = VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerPage = this.itemsPerPage[VariableSetup.defaultItemsPerPageNumbersIndex].key;
        this.tableData = [];
        this.tableHeaders = [];
    }

    ngOnInit() : void {
    	this.startSearch();
    }

    startSearch() : void {
    	this.searchFunction.apply(this.owner, [this.searchFilter, this.currentPage, this.currentItemsPerPage]).then( (result : any) => {
    		this.pageCount = result.pageCount;
    		this.generatePages();
    		this.tableData = result;
    	} );
    }

    generatePages () : void {
        this.pages = [];
        for (let i = 1; i <= this.pageCount; i++) {
            this.pages.push(i);
        }
    }

    setPage( pageNo : number) : void {
        if ( pageNo>=1 && pageNo<=this.pageCount ) {
            this.currentPage = pageNo;
            this.startSearch();
        }
    }

    emitRowClick( data : any ) : void {
    	this.rowClick.emit(data);
    }

}
