import { Component,Input, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import {TrainingNewList} from './training-new-getlist'
import {NewTrainingService} from'../services/getlist.training-new.service';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { URLSearchParams } from "@angular/http";
import { Course, CourseEvent, User, Location, Address, roles} from '../00_commons/interface';

declare var $ : any;


@Component({
	selector:'training-new',
	templateUrl: './training-new.component.html',
  providers: [NewTrainingService]
})

export class TrainingNewComponent  implements OnInit  { 


  training : CourseEvent;
  course: Course;
  user: User;
  location : Location;
  address: Address;

  errors : any;
  errorText: string;
  postText: string;
  trainingDateStartDate: string;
  trainingDateFinishTime: string;
  courseTypes: Array<Object>;
  trainersList : Array<Object>;
  courseEvents : Array<string>;
  trainerNameGet : Array<string>;
  addCourseType : Array<string>;
 
  newTrainingServiceInstance: NewTrainingService;
  // itt vannak a promisok

  //@Input()
    trainingEvents : Array<CourseEvent>=[];

  constructor (private newTrainingService: NewTrainingService, private userService:UserService, private router: Router) {

    
    this.address = {
      zip: null,
      city: "",
      street: "",
      number: ""
    }

    this.location = {
      address : this.address,
      room : ""
    }

    this.training = {
      name: "",
      eventId: "",
      description: "", //courseEventDescription
      teachers: [],//this.trainerNameGet,  //{name: "teacher's name", email: "csik@kk.hu", role: "role"}
      location: this.location,
      dateStart: "",
      dateFinish: ""
    }




    this.errorText = "";
    this.errors = {};
    this.newTrainingServiceInstance = newTrainingService;
    this.postText = "";
    this.trainingDateStartDate="";
    this.trainingDateFinishTime="";
    this.courseTypes = new Array<Object>();
    this.trainersList = new Array<Object>();
    this.courseEvents = new Array<string>();
    this.addCourseType= new Array<string>();
   

    this.course = {

      types: [],
      courseName: "",
      courseDescr: "",
      applicationDeadline: "",
      maxStudents: null,
      minStudents: null,
      courseDateStart: "",  
      courseDateFinish: "", 
      discount: "",
      price: null,
      priceDeadline: "",
      events: this.trainingEvents,
     

    }


  }

	ngOnInit () :void {
		document.body.className = "page-container-bg-solid";
        $( "#datepicker" ).datepicker({ 
            dateFormat: 'yy-mm-dd', 
            yearRange: '1910:2017',
            changeYear: true   
        });  
        this.getTypes();
        this.getTrainers();

 
        

	}

  addCourseEvents() : void {
    if(this.training.teachers == []){
     let nullTeacher=Array<User>();
     nullTeacher.push(  );
    }
    //console.log('Before adding training');
    let trainingCopy =JSON.parse(JSON.stringify(this.training))
    this.trainingEvents.push(trainingCopy);
    //console.log('After adding training');
    //console.log('Number of trainings: ' + this.trainingEvents.length);
    //window.scrollTo(10,document.body.scrollHeight);
    //$("html, body").animate({ scrollTop: "550px" });



  } 

  deleteCourseEvents(index: number): void{
    this.trainingEvents.splice(index, 1);
  }

  getTypes () :void {
    this.newTrainingServiceInstance.listTypes()
    .then( (response)=>{
      // a response-t bárhogy hívhatnám
        //console.log("successful http call for type list: " +response.json());
        this.courseTypes = response.json();
        // ez az Array miat kell, kitettük egy változóba
      })
      .catch( ( response )=>{
        console.log(response);
        this.errorText = "Hiba van a types listában!";
      });

  }

  getTrainers (): void {
    this.newTrainingServiceInstance.listGetTrainers()
    .then( (trainers)=>{
     // console.log("successful http call for trainers list: " +trainers.json());
     // for(let a of trainers.json()){
      //  console.log(a.name);
      //}
      this.trainersList = trainers.json();
      this.trainerNameGet = trainers.json();
      //console.log(this.trainerNameGet.length)
    })
    .catch( ( trainers ) =>{
      this.errorText = "Hiba van a trainers litában"
    })
  }

  trainingRequest() :void {
    this.errors = {};
    if (!this.userService.wordLengthValidator(this.course.courseName, 1)) {
      this.errors.courseName = 'Kérem adja meg a kurzus nevét!';
      $("html, body").animate({ scrollTop: "260px" }, 'slow');
    }
    if (!this.userService.wordLengthValidator(this.course.courseDescr, 1)) {
      this.errors.courseDescr = 'Kérem adja meg a kurzus leírását!';
      $("html, body").animate({ scrollTop: "260px" }, 'slow');
    }
    if (!this.userService.wordLengthValidator(this.training.name, 1)) {
      this.errors.name = 'Kérem adja meg a tréning nevét!';
      $("html, body").animate({ scrollTop: "860px" }, 'slow');
    }
    if (!this.userService.wordLengthValidator(this.training.description, 1)) {
      this.errors.description = 'Kérem adja meg a tréningleírását!';
      $("html, body").animate({ scrollTop: "860px" }, 'slow');
    }
    
    if (Object.keys(this.errors).length == 0) {

    let courseEvents= new Array<Object>();
    this.course.events.forEach((event) => {
      let trainers = new Array<string>();
      event.teachers.forEach((teacher) => {
         trainers.push(teacher.id); 
      });
      let typespush = new Array<string>();
      this.course.types.forEach((types) =>{
          typespush.push(types);
          });
      

      courseEvents.push({
        name: event.name,
        courseEventDescription: event.description,
        dateStart: event.dateStart,
        dateFinish: event.dateFinish,
        trainers: trainers,
        location: event.location
      });  
    });
    let requestObj = {
      types: this.course.types,
      name: this.course.courseName,
      description: this.course.courseDescr,
      discount: this.course.discount,
      maxStudents: ""+this.course.maxStudents,
      minStudents: ""+this.course.minStudents,
      applicationDeadline: this.course.applicationDeadline,
      price: ""+this.course.price,
      courseEvents: courseEvents
     };
   

    this.newTrainingServiceInstance.postTrainingRequest (requestObj)
    .then(response =>
    //  response.json().data as string
      //console.log("successful post trainers: " +response.json());
      //this.course = response.json().data
      alert("A " + this.course.courseName+" kurzus sikeresen rögzítve lett!")
      //this.postText= "A " + this.course.courseName+" kurzus sikeresen rögzítve lett!"
     
    )
    .catch( ( response )=>{
      try{
        alert("A hibás bevitel!");
        console.log(response);
        let json = response.json();
        this.errorText = "Hiba van a trainer post-ban!";
      }
      catch (e) {
      this.errorText = "Hiba van a trainer post-ban!";
      alert("A hibás bevitel!");
      }

    });
  }

  }

  addCourseTypeButton() : void {
    let coursePush = new Array<Object>();
     coursePush.push({
      text : this.addCourseType,

    });
    this.newTrainingServiceInstance.courseTypePush(coursePush)
    .then( ()=>{
      //this.router.navigate(['dashboard']);
      this.getTypes ();
      })
    .catch( ( response )=>{
      try{
        console.log(response);
        let json = response.json();
        this.errorText = "Hiba van a trainer post-ban!";
      }
      catch (e) {
      this.errorText = "Hiba van a trainer post-ban!";
      }

    });

  }

  backToCourseEvent(): void{
    $("html, body").animate({ scrollTop: $(document).height()-$(window).height() },'slow');
  }

  backToCourse(): void {
    $("html, body").animate({ scrollTop: "260px" }, 'slow');
  }

    /*data.append (  "location", 
      this.training.location.room,
      this.training.location.address.zip.toString(),
      this.training.location.address.city,
      this.training.location.address.street,
      this.training.location.address.number,

      );*/
   



  /*let data = new URLSearchParams();
  let training = {
    trainingName: this.training.trainingName,
    applicationDeadline: this.training.applicationDeadline,
    maxStudents: this.training.maxStudents,
    minStudents: this.training.minStudents,
    discount: this.training.discount,
    place: this.training.place,
    price: this.training.price,
    name : this.training.name,
    // nincs subject: this.training.subject,
    description: this.training.description,
   // courseEvents : [""] ,
   // nincs courseEventDescription : this.training.courseEventDescription,
    dateStart: this.training.dateStart,
    dateFinish: this.training.dateFinish,
   // trainers:[""],
    */
 



  /* */

/*this.http
    .post(Constants.BASEURL + 'REST/courses/types',
        data,{ withCredentials : true })*/

/*
  giveName () : void {
    this.newTrainingServiceInstance.getName("Name")
        .then( (response)=>{
        console.log("successful http name call: " +response.json().data);
        // data akkor kell ha nem array
      })
      .catch( ( response )=>{
        console.log(response);
        this.errorText = "Hiba van a névben!";
      });
  }


  giveDescription () : void {
        this.newTrainingServiceInstance.getDescription("description")
        .then( (response)=>{
        console.log("successful http description call: " +response.json().data);
      })
      .catch( ( response )=>{
        console.log(response);
        this.errorText = "Hiba van a description-ben!";
      });
  }

import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class myServiceClass {

private headers = new Headers({'Content-Type': 'application/json'});
constructor(private http: Http) {};

this.http
      .post(this._healthFromControllerJsonPostUrl, JSON.stringify('asdf'), {headers: this.headers})
      .toPromise()
      .then(response => response.json().data)
      .catch(this.handleError);

private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
 
}
 */

 /* let coursePush= new URLSearchParams(); 
      {
        coursePush.append("types", JSON.stringify(this.course.types) );
        coursePush.append("name", this.course.courseName);
        coursePush.append("description", this.course.courseDescr);
        coursePush.append("discount", this.course.discount);
        coursePush.append("maxStudents", this.course.maxStudents.toString());
        coursePush.append("minStudents", this.course.minStudents.toString());
        coursePush.append("applicationDeadline", this.course.applicationDeadline);
        coursePush.append("price", this.course.price.toString());*/
      
        /*coursePush.append("courseEvents[name]", ''+ this.training.name); //name
        coursePush.append("courseEvents[courseEventDescription]",''+ this.training.description); //courseEventDescription
        coursePush.append("courseEvents[dateStart]", ''+ this.training.dateStart); //dateStart
        coursePush.append("courseEvents[dateFinish]", ''+ this.training.dateFinish);  //dateFinish
        coursePush.append("courseEvents[trainers]", ''+ this.training.teachers);
     */
        
        //coursePush.append("courseEvents", JSON.stringify(this.course.events)); //name
        // A JSON.stringify a paraméterül kapott objektumot bejárja mélységben 
        //és json stringet generál az objektumból

        //coursePush.append("courseEvents", JSON.stringify(courseEvents)); //name
       
        /*coursePush.append("courseEvents[location[room]]", ''+ this.location.room)
     
        coursePush.append("training[location[address[zip]]]", ''+ this.location.address.zip.toString()); //zip
        coursePush.append("training[location[address[city]]]",  ''+this.location.address.city); //city
        coursePush.append("training[location[address[street]]]", ''+ this.location.address.street); // street
        coursePush.append("training[location[address[number]]]", ''+ this.location.address.number); //number 
      */
      //}


 }

