"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
require("rxjs/add/operator/toPromise");
var user_service_1 = require("../services/user.service");
var TrainingNewComponent = (function () {
    function TrainingNewComponent(newTrainingService, userService, router) {
        this.newTrainingService = newTrainingService;
        this.userService = userService;
        this.router = router;
        // itt vannak a promisok
        //@Input()
        this.trainingEvents = [];
        this.address = {
            zip: null,
            city: "",
            street: "",
            number: ""
        };
        this.location = {
            address: this.address,
            room: ""
        };
        this.training = {
            name: "",
            eventId: "",
            description: "",
            teachers: [],
            location: this.location,
            dateStart: "",
            dateFinish: ""
        };
        this.errorText = "";
        this.errors = {};
        this.newTrainingServiceInstance = newTrainingService;
        this.postText = "";
        this.trainingDateStartDate = "";
        this.trainingDateFinishTime = "";
        this.courseTypes = new Array();
        this.trainersList = new Array();
        this.courseEvents = new Array();
        this.addCourseType = new Array();
        this.course = {
            types: [],
            courseName: "",
            courseDescr: "",
            applicationDeadline: "",
            maxStudents: null,
            minStudents: null,
            courseDateStart: "",
            courseDateFinish: "",
            discount: "",
            price: null,
            priceDeadline: "",
            events: this.trainingEvents,
        };
    }
    TrainingNewComponent.prototype.ngOnInit = function () {
        document.body.className = "page-container-bg-solid";
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            yearRange: '1910:2017',
            changeYear: true
        });
        this.getTypes();
        this.getTrainers();
    };
    TrainingNewComponent.prototype.addCourseEvents = function () {
        if (this.training.teachers == []) {
            var nullTeacher = Array();
            nullTeacher.push();
        }
        //console.log('Before adding training');
        var trainingCopy = JSON.parse(JSON.stringify(this.training));
        this.trainingEvents.push(trainingCopy);
        //console.log('After adding training');
        //console.log('Number of trainings: ' + this.trainingEvents.length);
        //window.scrollTo(10,document.body.scrollHeight);
        //$("html, body").animate({ scrollTop: "550px" });
    };
    TrainingNewComponent.prototype.deleteCourseEvents = function (index) {
        this.trainingEvents.splice(index, 1);
    };
    TrainingNewComponent.prototype.getTypes = function () {
        var _this = this;
        this.newTrainingServiceInstance.listTypes()
            .then(function (response) {
            // a response-t bárhogy hívhatnám
            //console.log("successful http call for type list: " +response.json());
            _this.courseTypes = response.json();
            // ez az Array miat kell, kitettük egy változóba
        })
            .catch(function (response) {
            console.log(response);
            _this.errorText = "Hiba van a types listában!";
        });
    };
    TrainingNewComponent.prototype.getTrainers = function () {
        var _this = this;
        this.newTrainingServiceInstance.listGetTrainers()
            .then(function (trainers) {
            // console.log("successful http call for trainers list: " +trainers.json());
            // for(let a of trainers.json()){
            //  console.log(a.name);
            //}
            _this.trainersList = trainers.json();
            _this.trainerNameGet = trainers.json();
            //console.log(this.trainerNameGet.length)
        })
            .catch(function (trainers) {
            _this.errorText = "Hiba van a trainers litában";
        });
    };
    TrainingNewComponent.prototype.trainingRequest = function () {
        var _this = this;
        this.errors = {};
        if (!this.userService.wordLengthValidator(this.course.courseName, 1)) {
            this.errors.courseName = 'Kérem adja meg a kurzus nevét!';
            $("html, body").animate({ scrollTop: "260px" }, 'slow');
        }
        if (!this.userService.wordLengthValidator(this.course.courseDescr, 1)) {
            this.errors.courseDescr = 'Kérem adja meg a kurzus leírását!';
            $("html, body").animate({ scrollTop: "260px" }, 'slow');
        }
        if (!this.userService.wordLengthValidator(this.training.name, 1)) {
            this.errors.name = 'Kérem adja meg a tréning nevét!';
            $("html, body").animate({ scrollTop: "860px" }, 'slow');
        }
        if (!this.userService.wordLengthValidator(this.training.description, 1)) {
            this.errors.description = 'Kérem adja meg a tréningleírását!';
            $("html, body").animate({ scrollTop: "860px" }, 'slow');
        }
        if (Object.keys(this.errors).length == 0) {
            var courseEvents_1 = new Array();
            this.course.events.forEach(function (event) {
                var trainers = new Array();
                event.teachers.forEach(function (teacher) {
                    trainers.push(teacher.id);
                });
                var typespush = new Array();
                _this.course.types.forEach(function (types) {
                    typespush.push(types);
                });
                courseEvents_1.push({
                    name: event.name,
                    courseEventDescription: event.description,
                    dateStart: event.dateStart,
                    dateFinish: event.dateFinish,
                    trainers: trainers,
                    location: event.location
                });
            });
            var requestObj = {
                types: this.course.types,
                name: this.course.courseName,
                description: this.course.courseDescr,
                discount: this.course.discount,
                maxStudents: "" + this.course.maxStudents,
                minStudents: "" + this.course.minStudents,
                applicationDeadline: this.course.applicationDeadline,
                price: "" + this.course.price,
                courseEvents: courseEvents_1
            };
            this.newTrainingServiceInstance.postTrainingRequest(requestObj)
                .then(function (response) {
                //  response.json().data as string
                //console.log("successful post trainers: " +response.json());
                //this.course = response.json().data
                return alert("A " + _this.course.courseName + " kurzus sikeresen rögzítve lett!");
            })
                .catch(function (response) {
                try {
                    alert("A hibás bevitel!");
                    console.log(response);
                    var json = response.json();
                    _this.errorText = "Hiba van a trainer post-ban!";
                }
                catch (e) {
                    _this.errorText = "Hiba van a trainer post-ban!";
                    alert("A hibás bevitel!");
                }
            });
        }
    };
    TrainingNewComponent.prototype.addCourseTypeButton = function () {
        var _this = this;
        var coursePush = new Array();
        coursePush.push({
            text: this.addCourseType,
        });
        this.newTrainingServiceInstance.courseTypePush(coursePush)
            .then(function () {
            //this.router.navigate(['dashboard']);
            _this.getTypes();
        })
            .catch(function (response) {
            try {
                console.log(response);
                var json = response.json();
                _this.errorText = "Hiba van a trainer post-ban!";
            }
            catch (e) {
                _this.errorText = "Hiba van a trainer post-ban!";
            }
        });
    };
    TrainingNewComponent.prototype.backToCourseEvent = function () {
        $("html, body").animate({ scrollTop: $(document).height() - $(window).height() }, 'slow');
    };
    TrainingNewComponent.prototype.backToCourse = function () {
        $("html, body").animate({ scrollTop: "260px" }, 'slow');
    };
    return TrainingNewComponent;
}());
TrainingNewComponent = __decorate([
    core_1.Component({
        selector: 'training-new',
        templateUrl: './training-new.component.html',
        providers: [getlist_training_new_service_1.NewTrainingService]
    }),
    __metadata("design:paramtypes", [getlist_training_new_service_1.NewTrainingService, user_service_1.UserService, router_1.Router])
], TrainingNewComponent);
exports.TrainingNewComponent = TrainingNewComponent;
//# sourceMappingURL=training-new.component.js.map