import { Pipe, PipeTransform } from '@angular/core';
import { Location, Address }  from '../00_commons/interface';

@Pipe ({
	name : 'location'
})
export class LocationPipe implements PipeTransform {

	transform( loc : Location ) {
		return loc.address.zip + ' ' + loc.address.city + ', ' + loc.address.street + ' ' + loc.address.number;
	}
}