import { Component, OnInit, Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Course, CourseEvent } from '../00_commons/interface';
import { NewTrainingService } from '../services/getlist.training-new.service';
import 'rxjs/add/operator/toPromise';

declare var $ : any;

@Component({
  selector: 'trainings',
  templateUrl: './trainings.component.html'
})
export class TrainingsComponent implements OnInit { 

	allCourses: Course[];
  filterData : any;
  simpleFilterData: string;
  
  constructor(private courseService:NewTrainingService, private router: Router, private activatedRoute : ActivatedRoute) {
    this.simpleFilterData = "";
  	this.filterData = {
      name: '',
      type : '',
      dateStart : '',
      dateFinish : '',
      location : '',
      id :""
    };
    this.allCourses =[];

	}

  ngOnInit() : void {
  	document.body.className = "container-fluid";
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }); 
    
  }
  //Ah, milyen menő voltam... Régen... - Robi
  //Akkor ez most mi? - Gergely
  simpleSearch() :void {
    this.courseService.getFilteredCourseList( this.simpleFilterData ).then(courses => this.allCourses = courses); 
  }

  searchCourse() :void {
  	let filterString = '';
    if( this.filterData.name != "") {
		filterString = this.filterData.name;
    }else if( this.filterData.type != "") {
		filterString = this.filterData.type;
    }else if( this.filterData.startDate != "") {
    filterString = this.filterData.startDate;
    }else if( this.filterData.location != "") {
    filterString = this.filterData.location;
    }
		this.courseService.getFilteredCourseList( this.filterData ).then(courses => this.allCourses = courses);
  	
  	
  }

  complexSearch() :void {
    this.courseService.getcomplexSearch()
  }



}
