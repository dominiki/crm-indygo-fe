import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { Http, HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

import { Address, Location, Course, CourseEvent, roles, genders, KeyValueObj, User } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { NewTrainingService } from '../services/getlist.training-new.service';
import { MessageService } from '../services/message.service';
import 'rxjs/add/operator/toPromise';

declare var $ : any;

@Component({
    selector: 'user',
    templateUrl: './userDetails.component.html'
})
 
export class UserDetailsComponent {
	user: User; //selectedUser
	genders : KeyValueObj[];
	locations : Address;
	errors : any;
	currentUser: User;
	

	constructor(private http: Http, private router: Router, private userService:UserService, private courseService:NewTrainingService, private messageService : MessageService, private activatedRoute : ActivatedRoute) {

		this.user = {
			name: "",
			email: "",
			id: this.activatedRoute.snapshot.params['id'],
			gender: "",
			role: [],
			username: "",
			phone: "",
			address : [{ zip :"", city : "", street : "", number : "" }],
			birthDate: "",
			birthPlace: ""
		}
		this.currentUser = {
			name: "",
			email: "",
			id: "",
            role: []
		};
		this.genders = genders;
		this.errors = {};
		this.locations = [{ zip :"", city : "", street : "", number : "", other : "" }];
	}
	ngOnInit() : void {
		document.body.className = "profile";
		console.log( this.activatedRoute.snapshot.params['id']);
		this.userService.getGuestData(this.user.id)
			.then( user => this.user = user );	
		this.userService.getCurrentUser().then( user => this.currentUser = user ); 
		this.userService.getAddresses().then( addresses => this.locations = addresses.addresses );
		$( "#datepicker" ).datepicker({ 
			dateFormat: 'yy-mm-dd',
			changeYear:true,
			yearRange: '1910:2017'
		}); 

	}
	changeData() {
		//ide jön a módosítás, amit szeretnénk elküldeni json-ként...Balázs csinálja a backendjét.
		
	}
	hasRole( roleName : string) : boolean {
        let t : boolean = false;
        for (let role of this.currentUser.role) {
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    }
	deleteData() {

	}

}