import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../00_commons/interface';
import { UserService } from '../services/user.service';

@Component({
  selector: 'crm-header',
  templateUrl: './crm-header.component.html',
})
export class HeaderComponent implements OnInit {

	currentUser : User;

	constructor ( private userService : UserService, private router : Router ) {
		this.currentUser = {
			email: "",
			name: "",
			id: "",
			role : []
		};
	}

	ngOnInit() : void {
		this.userService.getCurrentUser()
		.then( user => this.currentUser = user)
		.catch( ( response ) => {
			if (response.status === 401) {
				this.router.navigate(['/login']);
			}
		});
	}

	hasRole( roleName : string) : boolean {
		let t : boolean = false;
		for (let role of this.currentUser.role) {
			if (role == roleName) {
				t = true;
				break;
			}
		}
		return t;
	}

	logOut() : void {
		
		this.userService.logOut()
		.catch( ( response ) => {
			console.log(response.status);
			console.clear(); // dob egy GET http://37.17.173.112:8080/crm_indygo-1.0-SNAPSHOT/login?logout 401 (Unauthorized) hibát, nem tudom miért, de kilép a rendszerből
			if (response.status === 401) {
				this.router.navigate(['/login']);
			}
			else if(response.status === 404) {
				this.router.navigate(['/login']);
			} else {
				alert("nagy gond van! a kilépésnél!")
			}
		});
		
	}
}


