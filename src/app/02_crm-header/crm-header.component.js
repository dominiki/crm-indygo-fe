"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_service_1 = require("../services/user.service");
var HeaderComponent = (function () {
    function HeaderComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.currentUser = {
            email: "",
            name: "",
            id: "",
            role: []
        };
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getCurrentUser()
            .then(function (user) { return _this.currentUser = user; })
            .catch(function (response) {
            if (response.status === 401) {
                _this.router.navigate(['/login']);
            }
        });
    };
    HeaderComponent.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.currentUser.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    HeaderComponent.prototype.logOut = function () {
        var _this = this;
        this.userService.logOut()
            .catch(function (response) {
            console.log(response.status);
            console.clear(); // dob egy GET http://37.17.173.112:8080/crm_indygo-1.0-SNAPSHOT/login?logout 401 (Unauthorized) hibát, nem tudom miért, de kilép a rendszerből
            if (response.status === 401) {
                _this.router.navigate(['/login']);
            }
            else if (response.status === 404) {
                _this.router.navigate(['/login']);
            }
            else {
                alert("nagy gond van! a kilépésnél!");
            }
        });
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'crm-header',
        templateUrl: './crm-header.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=crm-header.component.js.map