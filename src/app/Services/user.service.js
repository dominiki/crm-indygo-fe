"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var constant_1 = require("../constant");
var UserService = (function () {
    function UserService(http, router) {
        this.http = http;
        this.router = router;
        this.createProfileURL = "rest/client";
    }
    UserService.prototype.logOut = function () {
        return this.http.get(constant_1.Constants.ROOTURL + "logout", { withCredentials: true })
            .toPromise();
    };
    UserService.prototype.getStatus = function () {
        return this.http.get(constant_1.Constants.BASEURL + "rest/enum/status", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getUsersSimpleSearch = function (searchFilter, page, itemsPerPage) {
        //return this.http.get('../../assets/json/dummy_user_pager.json')
        return this.http.get(constant_1.Constants.BASEURL + "rest/client?searchexpression=" + searchFilter.s
            + "&pagenumber=" + page + "&maxperpage=" + itemsPerPage, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getUsersCombinedSearch = function (searchFilter, page, itemsPerPage) {
        return this.http.get('../../assets/json/dummy_user_pager.json')
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getClientsToDashboard = function (clickedP, itemPP) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/dashboard/clients?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getCoursesToDashboard = function (clickedP, itemPP) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/dashboard/courses?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.userFromRegistrationId = function (idText) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/register?id=" + idText, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            var json = response.json();
            if (json.errorCode && json.errorCode == "NotValidRegUrl") {
                return new Promise(function (resolve, reject) { reject(); });
            }
            else {
                return json;
            }
        })
            .catch(this.handleError);
    };
    UserService.prototype.wordLengthValidator = function (word, wordLength) {
        return word.length >= wordLength;
    };
    UserService.prototype.phoneNumberValidator = function (phoneNumber) {
        if (phoneNumber.length < 8) {
            return false;
        }
        var regexpPhone = /^\+[\d]*$/;
        return regexpPhone.test(phoneNumber);
    };
    UserService.prototype.emailValidator = function (email) {
        if (email.length < 9) {
            return false;
        }
        if (email.charAt(0) == "." || email.charAt(0) == "@" || email.charAt(email.length - 1) == "." || email.charAt(email.length - 1) == "@") {
            return false;
        }
        var atChar = false;
        var dotChar = false;
        var atIndex;
        for (var i = 1; i < email.length - 1; i++) {
            if (email.charAt(i) == "@" && !atChar) {
                atChar = true;
                atIndex = i;
            }
            else if (atChar && email.charAt(i) == "@") {
                return false;
            }
            else if (atChar && i - atIndex > 1 && email.charAt(i) == ".") {
                dotChar = true;
            }
        }
        return atChar && dotChar;
    };
    UserService.prototype.passwordValidator1 = function (pass) {
        if (pass.length < 6) {
            return false;
        }
        else {
            var smallChar = false;
            var bigChar = false;
            var numberChar = false;
            var regexpS = /[a-z]/;
            var regexpB = /[A-Z]/;
            var regexpN = /\d/;
            for (var i = 0; i < pass.length; i++) {
                if (regexpS.test(pass.charAt(i))) {
                    smallChar = true;
                }
                else if (regexpB.test(pass.charAt(i))) {
                    bigChar = true;
                }
                else if (regexpN.test(pass.charAt(i))) {
                    numberChar = true;
                }
                if (smallChar && bigChar && numberChar) {
                    return true;
                }
            }
            return false;
        }
    };
    UserService.prototype.getGuestList = function () {
        return this.http.get(constant_1.Constants.BASEURL + "rest/client/all", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        console.error('HIBA', error.status); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    UserService.prototype.getCurrentUser = function () {
        var _this = this;
        if (this.currentUser) {
            return new Promise(function (resolve, reject) {
                resolve(_this.currentUser);
            });
        }
        else {
            return this.http.get(constant_1.Constants.BASEURL + "rest/users/current", { withCredentials: true })
                .toPromise()
                .then(function (response) {
                _this.currentUser = response.json();
                return response.json();
            })
                .catch(this.handleError);
        }
    };
    UserService.prototype.getEveryUserList = function () {
        return this.http.get(constant_1.Constants.BASEURL + "rest/client/all", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getFilteredUserList = function (searched) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/client?searchexpression=" + searched, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getFilteredUserListComplex = function (searched) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/client?searchexpression=" + searched, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.deleteGuest = function (id) {
        return this.http.delete(constant_1.Constants.BASEURL + 'rest/client?id=' + id, { withCredentials: true })
            .toPromise()
            .then(function () { })
            .catch(this.handleError);
    };
    UserService.prototype.getGuestData = function (idText) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/clientby?id=" + idText, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            var json = response.json();
            if (json.errorCode && json.errorCode == "NotValidRegUrl") {
                return new Promise(function (resolve, reject) { reject(); });
            }
            else {
                return json;
            }
        })
            .catch(this.handleError);
    };
    UserService.prototype.getAddresses = function () {
        return this.http.get(constant_1.Constants.BASEURL + "rest/enum/addresses", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map