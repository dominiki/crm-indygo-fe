import { Injectable }    from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import { Address, Location, RegistrationStep2Response, User, UserSearch,
		CourseSearch, ClientCombinedSearchFilter, ClientSimpleSearchFilter } from '../00_commons/interface';
import { Constants } from '../constant';

@Injectable()
export class UserService {

	currentUser: User;
	createProfileURL: string = "rest/client";


	constructor( private http : Http, private router : Router ) {

	}

	logOut() : Promise<any> {	
		return this.http.get(
			Constants.ROOTURL+"logout",	
			{withCredentials: true})	
		.toPromise();
	}

	getStatus() : Promise<string[]> {
		return this.http.get(
			Constants.BASEURL+"rest/enum/status",	
			{withCredentials: true})	
		.toPromise()
		.then( (response) => {
			return response.json() as string[];
		})
		.catch(this.handleError);
	}

	getUsersSimpleSearch(searchFilter : ClientSimpleSearchFilter, page : number, itemsPerPage : number) :Promise<UserSearch> {	
		//return this.http.get('../../assets/json/dummy_user_pager.json')
		return this.http.get(			
			Constants.BASEURL+"rest/client?searchexpression="+ searchFilter.s
				+"&pagenumber="+ page +"&maxperpage=" + itemsPerPage,
			{withCredentials: true}
		)
		.toPromise()
		.then( (response) => {
			return response.json() as UserSearch;
		})
		.catch(this.handleError);
	}

	getUsersCombinedSearch(searchFilter : ClientCombinedSearchFilter, page : number, itemsPerPage : number) :Promise<UserSearch> {	
		return this.http.get('../../assets/json/dummy_user_pager.json')
/*		this.http.get(
			Constants.BASEURL+"rest/findclient?name="+ searchFilter.name +"&email="+ searchFilter.email
				+"&status="+ searchFilter.status +"&pagenumber="+ page +"&maxperpage=" + itemsPerPage,
			{withCredentials: true}
		) */
		.toPromise()
		.then( (response) => {
			return response.json() as UserSearch;
		})
		.catch(this.handleError);
	}

	getClientsToDashboard(clickedP : number, itemPP : number) : Promise<UserSearch> {
		return this.http.get(
			Constants.BASEURL+"rest/dashboard/clients?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">",
			{withCredentials: true})
		.toPromise()
		.then( (response) => {
			return response.json() as UserSearch;
		})
		.catch(this.handleError);
	}

	getCoursesToDashboard(clickedP : number, itemPP : number) : Promise<CourseSearch> {
		return this.http.get(
			Constants.BASEURL+"rest/dashboard/courses?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">",
			{withCredentials: true})
		.toPromise()
		.then( (response) => {
			return response.json() as CourseSearch;
		})
		.catch(this.handleError);
	}

	userFromRegistrationId(idText : string) : Promise<RegistrationStep2Response> {
		return this.http.get(
			Constants.BASEURL+"rest/register?id=" + idText,
			{withCredentials: true})
		.toPromise()
		.then( (response) => {
			let json = response.json();
			if (json.errorCode && json.errorCode == "NotValidRegUrl") {
				return new Promise<RegistrationStep2Response>((resolve, reject) => { reject(); });
			} else {
				return json as RegistrationStep2Response;
			}
		})
		.catch(this.handleError);
	}

	wordLengthValidator ( word : string, wordLength : number ) : boolean {
		return word.length >= wordLength;
	}

	phoneNumberValidator ( phoneNumber : string ) : boolean {  //első karakter + aztán csak számok és legalább 8
		if (phoneNumber.length < 8) {
			return false;
		}
    	var regexpPhone = /^\+[\d]*$/;
   		return regexpPhone.test(phoneNumber);
	}

	emailValidator( email : string ) : boolean {  //csak kukac és pont ellenőrzése
		if (email.length < 9) {
			return false;
		}
		if (email.charAt(0)=="." || email.charAt(0)=="@" || email.charAt(email.length-1)=="." || email.charAt(email.length-1)=="@") {
			return false;
		}
		var atChar : boolean = false;
		var dotChar : boolean = false;
		var atIndex : number;

		for (var i = 1; i<email.length-1; i++) {
			if (email.charAt(i)=="@" && !atChar) {
				atChar = true;
				atIndex = i;
			}else if (atChar && email.charAt(i)=="@") {
				return false;
			}else if ( atChar && i-atIndex>1 && email.charAt(i)==".") {
				dotChar = true;
			}	
		}
		return atChar && dotChar; 
	}

	passwordValidator1( pass : string ) : boolean { //legalább 6 karakter, legalább 1: szám, kisbetű, nagybetű	    let regexp = new RegExp('w+@\w+.\w+'),
	    if (pass.length<6) {
	    	return false;
	    } else {
	    	var smallChar : boolean = false;
	    	var bigChar : boolean = false;
	    	var numberChar : boolean = false;
	    	var regexpS = /[a-z]/;
	    	var regexpB = /[A-Z]/;
	    	var regexpN = /\d/;	    	
		    for (var i = 0; i < pass.length; i++) {
		    	if (regexpS.test(pass.charAt(i))) {
		    		smallChar = true;
		    	}else if (regexpB.test(pass.charAt(i))) {
		    		bigChar = true;
		    	}else if (regexpN.test(pass.charAt(i))) {
		    		numberChar = true;
		    	}
			    if (smallChar && bigChar && numberChar) {
			    	return true;
			    }
		    }
		    return false;
	    }
	}    

	getGuestList() :Promise<User[]> {
		return this.http.get(
			Constants.BASEURL+"rest/client/all",
			{withCredentials: true})
		.toPromise()
		.then((response)=>{
			return response.json() as User[];
		})
		.catch( this.handleError );
	}
	
	handleError(error: any): Promise<any> {
	    console.error('HIBA', error.status); // for demo purposes only
	    return Promise.reject(error.message || error);
	}

	getCurrentUser() : Promise<User> {
		if (this.currentUser) {
			return new Promise((resolve, reject) => {
				resolve(this.currentUser);
			});
			
		} else {
			return this.http.get(
				Constants.BASEURL + "rest/users/current",
				{withCredentials: true}
			)
			.toPromise()
			.then(( response ) => {
				this.currentUser = response.json();
				return response.json() as User;
			} )
			.catch( this.handleError );
		}

	}
	getEveryUserList() :Promise<User[]> {
		return this.http.get(
			Constants.BASEURL+"rest/client/all",
			{withCredentials: true})
		.toPromise()
		.then((response)=>{
			return response.json() as User[];
		})
		.catch( this.handleError );
	}
	getFilteredUserList( searched:string ) :Promise<User[]> {
		return this.http.get(
			Constants.BASEURL+"rest/client?searchexpression=" + searched,
			{withCredentials: true})
		.toPromise()
		.then((response)=>{
			return response.json() as User[];
		})
		.catch( this.handleError );
	}
	getFilteredUserListComplex( searched:string ) :Promise<User[]> {
		return this.http.get(
			Constants.BASEURL+"rest/client?searchexpression=" + searched,
			{withCredentials: true})
		.toPromise()
		.then((response)=>{
			return response.json() as User[];
		})
		.catch( this.handleError );
	}

  	deleteGuest( id : string ): Promise<void> {
  		return this.http.delete(
  			Constants.BASEURL +'rest/client?id=' + id,
  			{withCredentials: true})
  		.toPromise()
	    .then(() => {})
	    .catch(this.handleError);
  	}
  	getGuestData(idText : string) : Promise<User> {
		return this.http.get(
			Constants.BASEURL+"rest/clientby?id=" + idText,
			{withCredentials: true})
		.toPromise()
		.then( (response) => {
			let json = response.json();
			if (json.errorCode && json.errorCode == "NotValidRegUrl") {
				return new Promise<User>((resolve, reject) => { reject(); });
			} else {
				return json as User;
			}
		})
		.catch(this.handleError);
	}
	getAddresses() :Promise<any> {
		return this.http.get(
			Constants.BASEURL+"rest/enum/addresses",	
			{withCredentials: true}
			)		
		.toPromise()
		.then( (response) => {
			return response.json() as any;
		})
		.catch(this.handleError);
	}
  
}