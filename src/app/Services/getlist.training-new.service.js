"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var constant_1 = require("../constant");
//let headers = new Headers({ 'Content-Type': 'application/json' });
var NewTrainingService = (function () {
    function NewTrainingService(http) {
        this.http = http;
        this.trainingRequest = "rest/courses";
        this.addCourseRequest = "rest/courses/types";
    }
    NewTrainingService.prototype.listTypes = function () {
        // Promise<Response> a Responsive kell bele
        return this.http.get(constant_1.Constants.BASEURL + "rest/courses/types", { withCredentials: true })
            .toPromise();
    };
    /*
    getStartingCourseList() :Promise<Course[]> {
      return this.http.get(
        Constants.BASEURL+"rest/valami",
        {withCredentials: true})
      .toPromise()
      .then((response)=>{
        return response.json() as Course[];
      })
      .catch( this.handleError );
    }
  */
    NewTrainingService.prototype.getFilteredCourseList = function (searched) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/courses?searchexpression=" + searched, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    /*
    getEveryCourseList() :Promise<Course[]> {
      return this.http.get(
        Constants.BASEURL+"rest/courses/all",
        {withCredentials: true})
      .toPromise()
      .then((response)=>{
        return response.json() as Course[];
      })
      .catch( this.handleError );
    }
    */
    NewTrainingService.prototype.handleError = function (error) {
        console.error('HIBA', error.status); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    NewTrainingService.prototype.listGetTrainers = function () {
        return this.http.get(constant_1.Constants.BASEURL + "rest/trainers", { withCredentials: true })
            .toPromise();
    };
    NewTrainingService.prototype.postTrainingRequest = function (postObject) {
        return this.http.post(constant_1.Constants.BASEURL + this.trainingRequest, postObject, { withCredentials: true })
            .toPromise();
    };
    NewTrainingService.prototype.postTrainingRequestPut = function (postObject) {
        return this.http.put(constant_1.Constants.BASEURL + this.trainingRequest, postObject, { withCredentials: true })
            .toPromise();
    };
    NewTrainingService.prototype.TimeValidator = function (trainingDateFinishTime) {
        var timevalid = /(?:(?:(?:(?<hh>\d{1,2})[:.])?(?<mm>\d{1,2})[:.])?(?<ss>\d{1,2})[:.])?(?<s>\d{1,2})/;
        return timevalid.test(trainingDateFinishTime);
    };
    NewTrainingService.prototype.getTrainingData = function (idText) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/coursesbyid?courseId=" + idText, { withCredentials: true })
            .toPromise()
            .then(function (response) {
            var json = response.json();
            if (json.errorCode && json.errorCode == "NotValid") {
                return new Promise(function (resolve, reject) { reject(); });
            }
            else {
                return json;
            }
        })
            .catch(this.handleError);
    };
    NewTrainingService.prototype.courseTypePush = function (coursePush) {
        return this.http.post(constant_1.Constants.BASEURL + this.addCourseRequest, coursePush, { withCredentials: true })
            .toPromise();
    };
    NewTrainingService.prototype.getcomplexSearch = function () {
        return; //this.http.get("/rest/courses?type=<valami>&name=<valami>&dateStartBefore=<1800-01-01>&dateStartAfter=<2100-01-01>&dateFinishBefore=<1900-01-01>&dateFinishAfter=<2013-01-01>&location=<Rom>")
        //.toPromise();
    };
    NewTrainingService.prototype.listId = function (id) {
        return this.http.get(constant_1.Constants.BASEURL + "rest/coursesbyid?courseid=" + id, { withCredentials: true })
            .toPromise();
    };
    return NewTrainingService;
}());
NewTrainingService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NewTrainingService);
exports.NewTrainingService = NewTrainingService;
//# sourceMappingURL=getlist.training-new.service.js.map