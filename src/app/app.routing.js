"use strict";
var router_1 = require("@angular/router");
var login_component_1 = require("./01_login/login.component");
var forgotten_pw_component_1 = require("./01_forgottenpassword/forgotten-pw.component");
var registration_component_1 = require("./01_registration/registration.component");
var registration2_component_1 = require("./01_registration/registration2.component");
var success_reg_component_1 = require("./01_registration/success-reg.component");
var training_new_component_1 = require("./02_training_new/training-new.component");
var users_component_1 = require("./02_users/users.component");
var clients_component_1 = require("./02_clients/clients.component");
var setup_component_1 = require("./02_setup/setup.component");
var dashboard_component_1 = require("./02_dashboard/dashboard.component");
var profile_component_1 = require("./03_profile/profile.component");
var map_component_1 = require("./03_map/map.component");
var trainings_component_1 = require("./02_trainings/trainings.component");
var userDetails_component_1 = require("./03_UserDetails/userDetails.component");
var clientDetails_component_1 = require("./02_clientDetails/clientDetails.component");
var trainingDetails_component_1 = require("./02_trainingDetails/trainingDetails.component");
var appRoutes = [
    { path: '', redirectTo: '/login', pathMatch: "full" },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'forgotten-pw', component: forgotten_pw_component_1.ForgottenPwComponent },
    { path: 'registration', component: registration_component_1.RegistrationComponent },
    { path: 'registration/:id', component: registration2_component_1.RegistrationComponent2 },
    { path: 'successreg', component: success_reg_component_1.SuccessReg },
    { path: 'training-new', component: training_new_component_1.TrainingNewComponent },
    { path: 'users', component: users_component_1.UsersComponent },
    { path: 'clients', component: clients_component_1.ClientsComponent },
    { path: 'setup', component: setup_component_1.SetupComponent },
    { path: 'user/:id', component: userDetails_component_1.UserDetailsComponent },
    { path: 'client/:id', component: clientDetails_component_1.ClientDetailsComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent },
    { path: 'profile', component: profile_component_1.ProfileComponent },
    { path: 'map', component: map_component_1.MapComponent },
    { path: 'trainings', component: trainings_component_1.TrainingsComponent },
    { path: 'trainingDetails/:course.courseId', component: trainingDetails_component_1.TrainingDetails },
];
exports.routes = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map