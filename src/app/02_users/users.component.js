"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var setup_constans_1 = require("../00_commons/setup-constans");
var user_service_1 = require("../services/user.service");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var UsersComponent = (function () {
    function UsersComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.filteredUsers = [];
        this.filterData = {
            name: '',
            email: '',
            role: ''
        };
        this.filterDataSimple = '';
        this.user = {
            name: "",
            email: "",
            id: "",
            status: ""
        };
        this.pageCount = 0;
        this.roles = ["ROLE_ADMIN", "ROLE_ASSISTANT", "ROLE_TRAINER"];
        this.currentPage = 1;
        this.pages = [];
        this.itemsPerPage = setup_constans_1.VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerPage = this.itemsPerPage[setup_constans_1.VariableSetup.defaultItemsPerPageNumbersIndex].key;
        this.simplySearch = true;
    }
    UsersComponent.prototype.ngOnInit = function () {
        /*
        this.userService.getUsersCombinedSearch().then(result => {
            this.filteredUsers = result.users['1'];  //TODO ha backendes, akkor az [n] rész kijön
            this.pageCount = result.pageCount;
        });
        */
    };
    UsersComponent.prototype.pagesList = function () {
        var pages = [];
        for (var i = 1; i <= this.pageCount; i++) {
            pages.push(i);
        }
        return pages;
    };
    UsersComponent.prototype.getNeededPage = function (pageNo) {
        if (pageNo >= 1 && pageNo <= this.pageCount) {
            /*
            this.userService.getUsersCombinedSearch(this.currentPage, this.currentItemsPerPage, this.filterData).then(result => {
                this.filteredUsers = result.users[pageNo];  //TODO ha backendes, akkor az [n] rész kijön
                this.pageCount = result.pageCount;
            });
            */
            this.currentPage = pageNo;
        }
    };
    UsersComponent.prototype.simpleSearchUser = function () {
        /*
        this.userService.getUsersSimpleSearch(this.currentPage, this.currentItemsPerPage, this.filterDataSimple).then(result => {
            this.filteredUsers = result.users;
            this.pageCount = result.pageCount;
        });
        */
    };
    UsersComponent.prototype.combinedSearchUser = function () {
        /*
        this.userService.getUsersCombinedSearch(this.currentPage, this.currentItemsPerPage, this.filterData).then(result => {
            this.filteredUsers = result.users;
            this.pageCount = result.pageCount;
        });
        */
    };
    UsersComponent.prototype.changeSearch = function () {
        this.simplySearch = !this.simplySearch;
    };
    UsersComponent.prototype.navigateToUser = function (userId) {
        this.router.navigate(['/user', userId]); //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    };
    return UsersComponent;
}());
UsersComponent = __decorate([
    core_1.Component({
        selector: 'users',
        templateUrl: './users.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router])
], UsersComponent);
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.component.js.map