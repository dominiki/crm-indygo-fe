import { Component, OnInit, Injectable } from '@angular/core';

import { Address, Location, Course, CourseEvent, User, UserSearch } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { UserService }	from '../services/user.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'users',
	templateUrl: './users.component.html',
	
})

export class UsersComponent implements OnInit{

	filteredUsers: User[];  //lekérdezett felhasználók
	filterData : any;    //összetett keresési feltételek
	filterDataSimple : any;    //egyszerű keresési feltétel
	user: User;        //bejelentkezett user
    pageCount: number;
    roles: string[];
    currentPage: number;
    pages: number[];
    itemsPerPage: any[];
    currentItemsPerPage: number;
    simplySearch: boolean;

	constructor(private userService:UserService, private router:Router) {
		this.filteredUsers = [];    
		this.filterData = {
			name : '',
			email : '',
			role : ''
		};
		this.filterDataSimple = '';
		this.user = {
			name: "",
			email: "",
			id: "",
			status: ""
		};
        this.pageCount = 0;
        this.roles = ["ROLE_ADMIN", "ROLE_ASSISTANT", "ROLE_TRAINER"];
        this.currentPage = 1;
        this.pages = []; 
        this.itemsPerPage = VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerPage = this.itemsPerPage[VariableSetup.defaultItemsPerPageNumbersIndex].key;   
        this.simplySearch = true;
	}

	ngOnInit () : void {
		/*
		this.userService.getUsersCombinedSearch().then(result => {
            this.filteredUsers = result.users['1'];  //TODO ha backendes, akkor az [n] rész kijön
            this.pageCount = result.pageCount;
        });
        */
	}

    pagesList () : number[] {
        let pages : number[] = [];
        for (let i = 1; i <= this.pageCount; i++) {
            pages.push(i);
        }
        return pages;
    }

    getNeededPage( pageNo : number) : void {
        if ( pageNo>=1 && pageNo<=this.pageCount ) {
            /*
            this.userService.getUsersCombinedSearch(this.currentPage, this.currentItemsPerPage, this.filterData).then(result => {
                this.filteredUsers = result.users[pageNo];  //TODO ha backendes, akkor az [n] rész kijön  
                this.pageCount = result.pageCount;
            });
            */
            this.currentPage = pageNo;
        }
    }

	simpleSearchUser() : void {
		/*
		this.userService.getUsersSimpleSearch(this.currentPage, this.currentItemsPerPage, this.filterDataSimple).then(result => {
            this.filteredUsers = result.users;  
            this.pageCount = result.pageCount;
        });
        */
	}

	combinedSearchUser() : void {
		/*
		this.userService.getUsersCombinedSearch(this.currentPage, this.currentItemsPerPage, this.filterData).then(result => {
            this.filteredUsers = result.users;  
            this.pageCount = result.pageCount;
        });
        */
	}

	changeSearch() : void {
		this.simplySearch = !this.simplySearch;
	}

	navigateToUser(userId : number) : void {
		this.router.navigate(['/user', userId]);	//ilyenkor a /profile után egy per jel után az id-t hozzáadja
	}

}	
			
			