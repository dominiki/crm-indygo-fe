import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { Http, HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'successReg',
  templateUrl: './success-reg.component.html'
})
export class SuccessReg implements OnInit { 

	constructor( private http: Http, private router: Router){
	}

	ngOnInit() : void {
		setTimeout( () => { this.router.navigate(['login']); }, 5000);
	}

}