"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var interface_1 = require("../00_commons/interface");
var setup_constans_1 = require("../00_commons/setup-constans");
var constant_1 = require("../constant");
var user_service_1 = require("../services/user.service");
var message_service_1 = require("../services/message.service");
require("rxjs/add/operator/toPromise");
var RegistrationComponent2 = (function () {
    function RegistrationComponent2(http, router, userService, activatedRoute, applicationRef, messageService) {
        this.http = http;
        this.router = router;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.applicationRef = applicationRef;
        this.messageService = messageService;
        this.rulesText = setup_constans_1.VariableSetup.getIssueText();
        this.rulesChecked = false;
        this.locations = [
            { address: {}, selected: true }
        ];
        this.user = {
            name: "",
            email: "",
            id: this.activatedRoute.snapshot.params['id'],
            gender: "",
            role: [],
            username: "",
            phone: "",
            address: { zip: null, city: "", street: "", number: "", other: "" }
        };
        this.genders = interface_1.genders;
        this.password1 = "";
        this.password2 = "";
        this.errors = {};
    }
    RegistrationComponent2.prototype.ngOnInit = function () {
        var _this = this;
        document.body.className = "profile";
        //console.log( this.activatedRoute.snapshot.params['id'] );
        this.userService.userFromRegistrationId(this.user.id)
            .then(function (getUser) {
            Object.assign(_this.user, getUser.user);
            if (_this.hasRole("ROLE_TRAINER")) {
                _this.locations = getUser.locations;
            }
        })
            .catch(function () {
            _this.router.navigate(['/']);
        });
    };
    RegistrationComponent2.prototype.doRegister = function () {
        var _this = this;
        this.errors = {};
        if (!this.userService.wordLengthValidator(this.user.name, 1)) {
            this.errors.name = 'Kérem adja meg a nevét!';
        }
        if (!this.userService.wordLengthValidator(this.user.gender, 1)) {
            this.errors.gender = 'Kérem adja meg a nemét!';
        }
        if (!this.userService.phoneNumberValidator(this.user.phone)) {
            this.errors.phone = 'Érvénytelen telefonszám, kérem adja meg a telefonszámot a következő módon: "+3611234567"!';
        }
        if (!this.userService.wordLengthValidator(this.user.username, 6)) {
            this.errors.username = 'Nem megfelelő felhasználónév! (legalább 6 karakter)';
        }
        if (this.password1 != this.password2) {
            this.errors.password2 = 'Nem egyezik a 2 jelszó!';
        }
        if (!this.userService.passwordValidator1(this.password1)) {
            this.errors.password1 = 'Nem megfelelő jelszóformátum! (legalább 6 karakter, legyen benne kisbetű, nagybetű és szám)';
        }
        if (!this.userService.passwordValidator1(this.password2)) {
            this.errors.password2 = 'Nem megfelelő jelszóformátum! (legalább 6 karakter, legyen benne kisbetű, nagybetű és szám)';
        }
        if (Object.keys(this.errors).length == 0) {
            var postUser = JSON.parse(JSON.stringify(this.user));
            postUser.password = this.password1;
            postUser.generatedId = postUser.id;
            delete postUser.id;
            postUser.telephone = postUser.phone;
            delete postUser.phone;
            if (this.hasRole("ROLE_TRAINER")) {
                var notAcceptedLocations;
                for (var _i = 0, _a = this.locations; _i < _a.length; _i++) {
                    var loc = _a[_i];
                    if (!loc.selected) {
                        notAcceptedLocations.append(loc.address.id);
                    }
                }
                postUser.locations = notAcceptedLocations;
            }
            this.http.post(constant_1.Constants.BASEURL + 'rest/user', postUser, { withCredentials: true })
                .toPromise()
                .then(function () {
                _this.router.navigate(['successreg']);
            })
                .catch(function (response) {
                try {
                    var json = response.json();
                    if (json.errorCause == "userAlreadyExists") {
                        _this.errors.username = 'Már foglalt felhasználónév!';
                    }
                    else {
                        _this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
                    }
                }
                catch (e) {
                    _this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
                }
            });
        }
        else {
            this.applicationRef.tick(); //ha lefuttata a change detectiont és frissíti a domot, akkor utána megy tovább
            var formGroup = document.getElementsByClassName("form-group");
            for (var i = 0; formGroup.length > i; i++) {
                if (formGroup[i].classList.contains("has-error")) {
                    var rect = formGroup[i].getBoundingClientRect();
                    window.scrollBy(0, rect.top);
                    break;
                }
            }
        }
    };
    RegistrationComponent2.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.user.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    RegistrationComponent2.prototype.onBlurMethodName = function () {
        if (this.userService.wordLengthValidator(this.user.name, 1)) {
            delete this.errors.name;
        }
    };
    RegistrationComponent2.prototype.onBlurMethodGender = function () {
        if (this.userService.wordLengthValidator(this.user.gender, 1)) {
            delete this.errors.role;
        }
    };
    RegistrationComponent2.prototype.onBlurMethodPhone = function () {
        if (this.userService.phoneNumberValidator(this.user.phone)) {
            delete this.errors.phone;
        }
    };
    RegistrationComponent2.prototype.onBlurMethodUserName = function () {
        if (this.userService.wordLengthValidator(this.user.username, 6)) {
            delete this.errors.username;
        }
    };
    RegistrationComponent2.prototype.onBlurMethodPass1 = function () {
        if (this.userService.passwordValidator1(this.password1)) {
            delete this.errors.password1;
        }
    };
    RegistrationComponent2.prototype.onBlurMethodPass2 = function () {
        if (this.userService.passwordValidator1(this.password2)) {
            delete this.errors.password2;
        }
    };
    return RegistrationComponent2;
}());
RegistrationComponent2 = __decorate([
    core_1.Component({
        selector: 'registration2',
        templateUrl: './registration2.component.html'
    }),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router, user_service_1.UserService, router_1.ActivatedRoute, core_1.ApplicationRef, message_service_1.MessageService])
], RegistrationComponent2);
exports.RegistrationComponent2 = RegistrationComponent2;
//# sourceMappingURL=registration2.component.js.map