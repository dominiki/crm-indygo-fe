"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var interface_1 = require("../00_commons/interface");
var constant_1 = require("../constant");
var user_service_1 = require("../services/user.service");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
var message_service_1 = require("../services/message.service");
require("rxjs/add/operator/toPromise");
var ProfileComponent = (function () {
    function ProfileComponent(http, router, userService, courseService, applicationRef, messageService) {
        this.http = http;
        this.router = router;
        this.userService = userService;
        this.courseService = courseService;
        this.applicationRef = applicationRef;
        this.messageService = messageService;
        $('#successModal').modal({ show: false });
        this.user = {
            name: "",
            email: "",
            gender: "",
            birthPlace: "",
            birthDate: "",
            phone: "",
            address: { zip: null, city: "", street: "", number: "", other: "" },
            role: [],
            otherInformation: "",
            status: ""
        };
        this.status = [];
        this.startDateContact = "";
        this.education = "";
        this.job = "";
        this.currentUser = {
            name: "",
            email: "",
            id: "",
            role: []
        };
        this.genders = interface_1.genders;
        this.errors = {};
        this.allCourses = [];
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        document.body.className = "container-fluid";
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            yearRange: '1910:2017',
            changeYear: true
        });
        $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
        this.userService.getStatus().then(function (status) { return _this.status = status; });
        this.userService.getCurrentUser().then(function (user) { return _this.currentUser = user; });
    };
    ProfileComponent.prototype.navigate = function () {
        this.router.navigate(['dashboard']);
    };
    ProfileComponent.prototype.register = function () {
        var _this = this;
        this.errors = {};
        if (!this.userService.wordLengthValidator(this.user.name, 1)) {
            this.errors.name = 'Kérem adja meg a nevét!';
        }
        if (!this.userService.emailValidator(this.user.email)) {
            this.errors.email = 'Érvénytelen e-mail cím!';
        }
        if (!this.userService.phoneNumberValidator(this.user.phone)) {
            this.errors.phone = 'Érvénytelen telefonszám, kérem adja meg a telefonszámot a követekző módon: "+361234567"!';
        }
        if (!this.userService.wordLengthValidator(this.user.gender, 1)) {
            this.errors.gender = 'Kérem adja meg a nemét!';
        }
        if (Object.keys(this.errors).length == 0) {
            var json = {
                name: this.user.name,
                email: this.user.email,
                gender: this.user.gender,
                phone: this.user.phone,
                education: this.education,
                birthPlace: this.user.birthPlace,
                birthDate: this.user.birthDate,
                startDateContact: this.startDateContact,
                zip: this.user.address.zip,
                city: this.user.address.city,
                street: this.user.address.street,
                number: this.user.address.number,
                other: this.user.address.other,
                otherInformation: this.user.otherInformation,
                status: this.user.status,
                job: this.job
            };
            this.http.post(constant_1.Constants.BASEURL + 'rest/client', json, { withCredentials: true })
                .toPromise()
                .then(function (response) {
                //this.router.navigate(['dashboard']);
                $('#successModal').modal('show');
                _this.user.id = response.json().data.clientId;
            })
                .catch(function (response) {
                var json = response.json();
                if (json.errorCause == "userAlreadyExists") {
                    _this.errors.username = 'Már foglalt felhasználónév!';
                }
            });
        }
        else {
            this.applicationRef.tick(); //ha lefuttata a change detectiont és frissíti a domot, akkor utána megy tovább
            var formGroup = document.getElementsByClassName("form-group");
            for (var i = 0; formGroup.length > i; i++) {
                if (formGroup[i].classList.contains("has-error")) {
                    var rect = formGroup[i].getBoundingClientRect();
                    window.scrollBy(0, rect.top);
                    break;
                }
            }
        }
    };
    ProfileComponent.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.currentUser.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    ProfileComponent.prototype.onBlurMethodName = function () {
        if (this.userService.wordLengthValidator(this.user.name, 1)) {
            delete this.errors.name;
        }
    };
    ProfileComponent.prototype.onBlurMethodEmail = function () {
        if (this.userService.emailValidator(this.user.email)) {
            delete this.errors.email;
        }
    };
    ProfileComponent.prototype.onBlurMethodPhone = function () {
        if (this.userService.phoneNumberValidator(this.user.phone)) {
            delete this.errors.phone;
        }
    };
    ProfileComponent.prototype.onBlurMethodGender = function () {
        if (this.userService.wordLengthValidator(this.user.gender, 1)) {
            delete this.errors.gender;
        }
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    core_1.Component({
        selector: 'profile',
        templateUrl: './profile.component.html'
    }),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router, user_service_1.UserService, getlist_training_new_service_1.NewTrainingService, core_1.ApplicationRef, message_service_1.MessageService])
], ProfileComponent);
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map